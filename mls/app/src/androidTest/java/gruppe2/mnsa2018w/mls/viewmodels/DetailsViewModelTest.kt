package gruppe2.mnsa2018w.mls.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import gruppe2.mnsa2018w.mls.data.AppDatabase
import gruppe2.mnsa2018w.mls.data.ReportRepository
import gruppe2.mnsa2018w.mls.utilities.getValue
import gruppe2.mnsa2018w.mls.utilities.testReport
import gruppe2.mnsa2018w.mls.utilities.testReports
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.hamcrest.Matchers.equalTo
import org.junit.*

class DetailsViewModelTest {
    private lateinit var appDatabase: AppDatabase
    private lateinit var reportRepo: ReportRepository
    private lateinit var viewModel: DetailsViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        reportRepo = ReportRepository.getInstance(appDatabase.reportDao())

        CoroutineScope(Dispatchers.IO).launch {
            reportRepo.insertReport(testReports[0])
        }
        Thread.sleep(400)

        viewModel = DetailsViewModel(reportRepo, testReport.reportId)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    fun testDefaultId() {
        Assert.assertThat(getValue(viewModel.report).reportId, equalTo(testReport.reportId))
    }
}