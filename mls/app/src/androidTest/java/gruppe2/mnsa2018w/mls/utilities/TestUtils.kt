package gruppe2.mnsa2018w.mls.utilities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.widget.Toolbar
import androidx.test.espresso.intent.matcher.IntentMatchers
import gruppe2.mnsa2018w.mls.data.Gps
import gruppe2.mnsa2018w.mls.data.Mls
import gruppe2.mnsa2018w.mls.data.Report
import org.hamcrest.Matcher
import org.hamcrest.Matchers

// Data is based on values of report-data-fixture.json
val testReports = arrayListOf(
    Report(
        reportId = 1,
        date = "Thu Jan 1 07:45:40 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            17.597f
        ),
        mls = Mls(
            48.219,
            16.495,
            25000f
        ),
        positionDifference = 10013.871f,
        mlsRequest = """
                {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -73,
        "mRsrq": -6,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 1
      },
      "mRegistered": true,
      "mTimeStamp": 118784013403793,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 199393004516,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -51
    },
    {
      "age": 199393004423,
      "macAddress": "fc:ec:da:8a:5a:12",
      "signalStrength": -51
    },
    {
      "age": 199393004524,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -51
    },
    {
      "age": 199393004459,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -56
    }
  ]
}
            """.trimIndent(),
        mlsResult = """
                  {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
            """.trimIndent()
    ),
    Report(
        reportId = 2,
        date = "Thu Jan 2 08:17:36 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            16.3082,
            48.2428,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388460,
      "macAddress": "38:43:7d:5d:64:37",
      "signalStrength": -67
    },
    {
      "age": 201309388475,
      "macAddress": "8e:04:ff:14:ca:4d",
      "signalStrength": -69
    }
    ]
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 3,
        date = "Thu Jan 3 08:17:36 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            16.3082,
            48.2428,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388460,
      "macAddress": "38:43:7d:5d:64:37",
      "signalStrength": -67
    },
    {
      "age": 201309388475,
      "macAddress": "8e:04:ff:14:ca:4d",
      "signalStrength": -69
    }
    ]
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 4,
        date = "Thu Jan 4 08:17:37 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 5,
        date = "Thu Jan 5 08:17:37 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 6,
        date = "Thu Jan 6 08:17:37 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 7,
        date = "Thu Jan 7 08:17:37 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 8,
        date = "Thu Jan 8 08:17:37 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 9,
        date = "Thu Jan 9 08:17:37 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 10,
        date = "Thu Jan 10 08:17:40 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 11,
        date = "Thu Jan 11 08:17:40 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 12,
        date = "Thu Jan 12 08:17:40 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 13,
        date = "Thu Jan 13 08:17:40 GMT+01:00 2019",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    ),
    Report(
        reportId = 14,
        date = "The One",
        gps = Gps(
            48.176,
            16.377,
            15.594f
        ),
        mls = Mls(
            48.1521,
            16.3878,
            5000f
        ),
        positionDifference = 2726.6743f,
        mlsRequest = """
            {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -70,
        "mRsrq": -11,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": true,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    },
    {
      "mCellIdentityLte": {
        "mCi": 2147483647,
        "mEarfcn": 1875,
        "mMcc": 2147483647,
        "mMnc": 2147483647,
        "mPci": 493,
        "mTac": 2147483647
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -78,
        "mRsrq": -17,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 2147483647
      },
      "mRegistered": false,
      "mTimeStamp": 120700450195852,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 201309388437,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388449,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -47
    },
    {
      "age": 201309388497,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -52
    },
    {
      "age": 201309388548,
      "macAddress": "38:43:7d:5d:64:0e",
      "signalStrength": -64
    },
    {
      "age": 201309388570,
      "macAddress": "90:5c:44:f9:13:f8",
      "signalStrength": -93
    }
  ]
}
        """.trimIndent(),
        mlsResult = """
            {
  "accuracy": 5000,
  "location": {
    "lat": 48.1521,
    "lng": 16.3878
  }
}
        """.trimIndent()
    )
)

val testReport = testReports[0]

/**
 * Returns the content description for the navigation button view in the toolbar.
 */
fun getToolbarNavigationContentDescription(activity: Activity, toolbarId: Int) =
    activity.findViewById<Toolbar>(toolbarId).navigationContentDescription as String

/**
 * Simplify testing Intents with Chooser
 *
 * @param matcher the actual intent before wrapped by Chooser Intent
 */
fun chooser(matcher: Matcher<Intent>): Matcher<Intent> = Matchers.allOf(
    IntentMatchers.hasAction(Intent.ACTION_CHOOSER),
    IntentMatchers.hasExtra(Matchers.`is`(Intent.EXTRA_INTENT), matcher)
)