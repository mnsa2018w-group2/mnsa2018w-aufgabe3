package gruppe2.mnsa2018w.mls.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import gruppe2.mnsa2018w.mls.utilities.getValue
import gruppe2.mnsa2018w.mls.utilities.testReports
import org.hamcrest.Matchers.equalTo
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ReportDaoTest {
    private lateinit var database: AppDatabase
    private lateinit var reportDao: ReportDao
    private val reportA = testReports[0]
    private val reportB = testReports[1]
    private val reportC = testReports[2]

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        reportDao = database.reportDao()
        reportDao.insertAll(listOf(reportA, reportB, reportC))
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun getReport() {
        assertThat(getValue(reportDao.getReport(reportA.reportId)), equalTo(reportA))
    }

    @Test
    fun getReports() {
        val reportList = getValue(reportDao.getReports())
        assertThat(reportList.size, equalTo(3))

        assertThat(reportList[0], equalTo(reportA))
        assertThat(reportList[1], equalTo(reportB))
        assertThat(reportList[2], equalTo(reportC))
    }

    @Test
    fun insertReport() {
        val reportD = Report(
            date = "Thu Jan 10 07:45:40 GMT+01:00 2019",
            gps = Gps(
                48.176,
                16.377,
                17.597f
            ),
            mls = Mls(
                48.219,
                16.495,
                25000f
            ),
            positionDifference = 10013.871f,
            mlsRequest = "",
            mlsResult = ""
        )
        reportDao.insertReport(reportD)
        val reportList2 = getValue(reportDao.getReports())
        assertThat(reportList2.size, equalTo(4))
    }


    @Test
    fun insertAllReports() {
        reportDao.deleteAllReports()
        reportDao.insertAll(listOf(reportA, reportB, reportC))
        assertThat(getValue(reportDao.getReports()).size, equalTo(3))
    }

    @Test
    fun insertReportSecondTimeShouldBeIgnored() {
        val oldReportList = getValue(reportDao.getReports())
        assertThat(oldReportList.size, equalTo(3))

        reportDao.insertReport(testReports[0])

        val newReportList = getValue(reportDao.getReports())
        assertThat(newReportList.size, equalTo(3))
    }

    @Test
    fun deleteReport() {
        val reportList = getValue(reportDao.getReports())
        reportDao.deleteReport(reportList[0])
        val reportList2 = getValue(reportDao.getReports())
        assertThat(reportList2.size, equalTo(2))
    }

    @Test
    fun deleteAllReports() {
        reportDao.deleteAllReports()
        val reportList0 = getValue(reportDao.getReports())
        assertThat(reportList0.size, equalTo(0))
    }
}