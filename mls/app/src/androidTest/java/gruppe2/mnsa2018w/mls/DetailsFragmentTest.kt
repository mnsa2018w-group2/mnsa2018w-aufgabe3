package gruppe2.mnsa2018w.mls

import androidx.arch.core.executor.testing.CountingTaskExecutorRule
import androidx.navigation.findNavController
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import gruppe2.mnsa2018w.mls.data.AppDatabase
import gruppe2.mnsa2018w.mls.data.ReportRepository
import gruppe2.mnsa2018w.mls.utilities.testReports
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DetailsFragmentTest {

    @get:Rule
    var countingTaskExecutorRule = CountingTaskExecutorRule()

    private val testReport = testReports[0]
    private lateinit var repository: ReportRepository

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        repository =
                ReportRepository.getInstance(AppDatabase.getInstance(activityTestRule.activity.applicationContext).reportDao())

        CoroutineScope(Dispatchers.IO).launch {
            repository.insertReport(testReport)
        }
    }

    @After
    fun cleanUp() {
        CoroutineScope(Dispatchers.IO).launch {
            repository.removeAllReports()
        }
    }

    @Before
    fun jumpToReportDetailsFragment() {
        activityTestRule.activity.apply {
            runOnUiThread {
                val bundle = DetailsFragmentArgs.Builder(testReport.reportId).build().toBundle()
                findNavController(R.id.mls_nav_fragment).navigate(R.id.details_fragment, bundle)
            }
        }
    }

    @Test
    fun testDeleteReport() {
        onView(withId(R.id.action_delete)).perform(click())
        onView(ViewMatchers.withText(testReport.date)).check(doesNotExist())
    }
}