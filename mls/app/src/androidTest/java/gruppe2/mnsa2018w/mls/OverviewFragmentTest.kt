package gruppe2.mnsa2018w.mls

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import gruppe2.mnsa2018w.mls.data.AppDatabase
import gruppe2.mnsa2018w.mls.data.ReportRepository
import gruppe2.mnsa2018w.mls.utilities.CustomMatchers
import gruppe2.mnsa2018w.mls.utilities.testReport
import gruppe2.mnsa2018w.mls.utilities.testReports
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class OverviewFragmentTest {

    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        val repository =
            ReportRepository.getInstance(AppDatabase.getInstance(activityTestRule.activity.applicationContext).reportDao())

        CoroutineScope(Dispatchers.IO).launch {
            repository.insertReport(testReports[0])
            repository.insertReport(testReports[1])
        }
        Thread.sleep(400)
    }

    @After
    fun cleanUp() {
        val repository =
            ReportRepository.getInstance(AppDatabase.getInstance(activityTestRule.activity.applicationContext).reportDao())
        CoroutineScope(Dispatchers.IO).launch {
            repository.removeAllReports()
        }
        Thread.sleep(100)
    }

    @Test
    fun testInitiallyShows2Reports() {
        onView(withId(R.id.report_list)).check(ViewAssertions.matches(CustomMatchers.withItemCount(2)))
    }

    @Test
    fun addsANewReport() {
        onView(withId(R.id.addReportButton)).perform(click())
        Thread.sleep(1000)
        onView(withId(R.id.report_list)).check(ViewAssertions.matches(CustomMatchers.withItemCount(3)))

    }

    @Test
    fun testDeleteAllReports() {
        Espresso.openContextualActionModeOverflowMenu()
        onView(ViewMatchers.withText("Delete all reports")).perform(ViewActions.click())
        onView(withId(R.id.report_list)).check(ViewAssertions.matches(CustomMatchers.withItemCount(0)))
    }

    @Test
    fun testOpenDetails() {
        onView(withText(testReport.date)).perform(click())
        onView(withId(R.id.Timestamp)).check(matches(isDisplayed()))
    }

    @Test
    fun testTheLastReportIsNotYetShown() {
        addReports()
        onView(withText(testReports[13].date)).check(doesNotExist())
    }

    @Test
    fun testTheLastReportIsShown() {
        addReports()
        onView(withId(R.id.report_list))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(13))
        onView(withText(testReports[13].date)).check(matches(isDisplayed()))
    }

    private fun addReports() {
        val repository =
            ReportRepository.getInstance(AppDatabase.getInstance(activityTestRule.activity.applicationContext).reportDao())

        CoroutineScope(Dispatchers.IO).launch {
            repository.insertReport(testReports[2])
            repository.insertReport(testReports[3])
            repository.insertReport(testReports[4])
            repository.insertReport(testReports[5])
            repository.insertReport(testReports[6])
            repository.insertReport(testReports[7])
            repository.insertReport(testReports[8])
            repository.insertReport(testReports[9])
            repository.insertReport(testReports[10])
            repository.insertReport(testReports[11])
            repository.insertReport(testReports[12])
            repository.insertReport(testReports[13])
        }
        Thread.sleep(500)
    }


}