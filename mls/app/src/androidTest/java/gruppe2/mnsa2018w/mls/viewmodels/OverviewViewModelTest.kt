package gruppe2.mnsa2018w.mls.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import gruppe2.mnsa2018w.mls.data.AppDatabase
import gruppe2.mnsa2018w.mls.data.ReportRepository
import gruppe2.mnsa2018w.mls.utilities.getValue
import gruppe2.mnsa2018w.mls.utilities.testReports
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.hamcrest.Matchers.equalTo
import org.junit.*
import org.junit.Assert.assertThat

class OverviewViewModelTest {
    private lateinit var appDatabase: AppDatabase
    private lateinit var viewModel: OverviewViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()

        val reportRepo = ReportRepository.getInstance(appDatabase.reportDao())
        CoroutineScope(Dispatchers.IO).launch {
            reportRepo.insertReport(testReports[0])
            reportRepo.insertReport(testReports[1])
        }
        Thread.sleep(400)

        viewModel = OverviewViewModel(reportRepo)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }


    @Test
    fun testDeleteReports() {
        val oldReportList = getValue(viewModel.getReports())
        val oldReportListSize = oldReportList.size
        viewModel.deleteAllReports()
        Thread.sleep(300)
        val newReportList = getValue(viewModel.getReports())
        val newReportListSize = newReportList.size

        Assert.assertTrue(oldReportListSize >= 0)
        assertThat(newReportListSize, equalTo(0))
    }
}
