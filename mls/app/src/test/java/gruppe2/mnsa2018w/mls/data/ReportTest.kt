package gruppe2.mnsa2018w.mls.data

import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test

class ReportTest {
    private lateinit var report: Report

    @Before
    fun setUp() {
        report = Report(
            reportId = 1,
            date = "Thu Jan 10 07:45:40 GMT+01:00 2019",
            gps = Gps(
                48.176,
                16.377,
                17.59f
            ),
            mls = Mls(
                48.219,
                16.495,
                25000.0f
            ),
            positionDifference = 10013.871f,
            mlsRequest = """
                {
  "cellTowers": [
    {
      "mCellIdentityLte": {
        "mCi": 28786443,
        "mEarfcn": 1875,
        "mMcc": 232,
        "mMnc": 5,
        "mPci": 132,
        "mTac": 10217
      },
      "mCellSignalStrengthLte": {
        "mCqi": 2147483647,
        "mRsrp": -73,
        "mRsrq": -6,
        "mRssnr": 2147483647,
        "mSignalStrength": 31,
        "mTimingAdvance": 1
      },
      "mRegistered": true,
      "mTimeStamp": 118784013403793,
      "mTimeStampType": 3
    }
  ],
  "wifiAccessPointDtos": [
    {
      "age": 199393004516,
      "macAddress": "fc:ec:da:8b:5a:12",
      "signalStrength": -51
    },
    {
      "age": 199393004423,
      "macAddress": "fc:ec:da:8a:5a:12",
      "signalStrength": -51
    },
    {
      "age": 199393004524,
      "macAddress": "fe:ec:da:8b:5a:12",
      "signalStrength": -51
    },
    {
      "age": 199393004459,
      "macAddress": "fe:ec:da:8a:5a:12",
      "signalStrength": -56
    }
  ]
}
            """.trimIndent(),
            mlsResult = """
                  {
 	"accuracy": 25000,
 	"location": {
 		"lat": 48.219,
 		"lng": 16.495
 	}
 }
            """.trimIndent()
        )
    }

    @Test
    fun test_gps_toString() {
        assertEquals("48.176°N, 16.377°E", report.gps.toString())
    }

    @Test
    fun test_mls_toString() {
        assertEquals("48.219°N, 16.495°E", report.mls.toString())
    }
}