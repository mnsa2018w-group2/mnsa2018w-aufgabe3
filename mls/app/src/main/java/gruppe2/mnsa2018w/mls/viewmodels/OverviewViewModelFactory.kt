package gruppe2.mnsa2018w.mls.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import gruppe2.mnsa2018w.mls.data.ReportRepository

class OverviewViewModelFactory(
    private val repository: ReportRepository
) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>) = OverviewViewModel(repository) as T
}
