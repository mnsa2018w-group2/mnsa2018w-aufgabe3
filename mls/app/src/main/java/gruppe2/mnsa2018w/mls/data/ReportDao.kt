package gruppe2.mnsa2018w.mls.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ReportDao {
    @Query("SELECT * FROM reports ORDER BY id")
    fun getReports(): LiveData<List<Report>>

    @Query("SELECT * FROM reports WHERE id = :reportId")
    fun getReport(reportId: Int): LiveData<Report>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertReport(report: Report)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(reports: List<Report>)

    @Delete
    fun deleteReport(report: Report)

    @Query("DELETE FROM reports")
    fun deleteAllReports()
}