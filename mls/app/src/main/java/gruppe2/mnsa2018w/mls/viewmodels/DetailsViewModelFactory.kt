package gruppe2.mnsa2018w.mls.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import gruppe2.mnsa2018w.mls.data.ReportRepository

class DetailsViewModelFactory(
    private val reportRepository: ReportRepository,
    private val reportId: Int
) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DetailsViewModel(reportRepository, reportId) as T
    }
}
