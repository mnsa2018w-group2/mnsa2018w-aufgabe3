package gruppe2.mnsa2018w.mls.services

import android.annotation.SuppressLint
import android.app.IntentService
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.os.Handler
import android.os.Looper
import android.telephony.*
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import gruppe2.mnsa2018w.mls.BuildConfig
import gruppe2.mnsa2018w.mls.R
import gruppe2.mnsa2018w.mls.data.*
import gruppe2.mnsa2018w.mls.dtos.CellTowerRequestDto
import gruppe2.mnsa2018w.mls.dtos.LocationResponseDto
import gruppe2.mnsa2018w.mls.dtos.MlsRequestDto
import gruppe2.mnsa2018w.mls.dtos.WifiRequestDto
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class LocationScanService : IntentService("LocationScanService") {
    private val context = this
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onHandleIntent(intent: Intent?) {
        handleGPSScan()
        handleWifiScan()
    }

    private fun handleGPSScan() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    }

    private fun handleWifiScan() {
        val wifiManager = application.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiList = ArrayList<WifiRequestDto>()

        if (!wifiManager.isWifiEnabled) {
            wifiManager.isWifiEnabled = true
        }

        val wifiBroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent?) {
                val resultList = wifiManager.scanResults as ArrayList<ScanResult>
                resultList.mapTo(wifiList) { WifiRequestDto(it.BSSID, it.timestamp, it.level) }
                applicationContext.unregisterReceiver(this)
                prepareReport(wifiList)
            }
        }

        applicationContext.registerReceiver(
            wifiBroadcastReceiver,
            IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
        )

        wifiManager.startScan()
    }

    @SuppressLint("MissingPermission")
    private fun getCellTowerDataList(telephonyManager: TelephonyManager): MutableList<CellTowerRequestDto> {
        val cellInfoArray = telephonyManager.allCellInfo
        val cellTowers: ArrayList<CellTowerRequestDto> = ArrayList()
        cellInfoArray.map { cellInfo ->
            val radioType: String
            var mobileCountryCode = 0
            var mobileNetworkCode = 0
            var locationAreaCode = 0
            var cellId = 0
            var signalStrength = 0
            var timingAdvance = 0

            when (cellInfo) {
                is CellInfoGsm -> {
                    radioType = "gsm"
                    signalStrength = cellInfo.cellSignalStrength.dbm
                    val identity = cellInfo.cellIdentity

                    if (identity.mcc != Integer.MAX_VALUE) {
                        mobileCountryCode = identity.mcc
                    }

                    if (identity.mnc != Integer.MAX_VALUE) {
                        mobileNetworkCode = identity.mnc
                    }

                    if (identity.lac != Integer.MAX_VALUE) {
                        locationAreaCode = identity.lac
                    }

                    if (identity.cid != Integer.MAX_VALUE) {
                        cellId = identity.cid
                    }
                }

                is CellInfoCdma -> {
                    radioType = "cdma"
                    signalStrength = cellInfo.cellSignalStrength.dbm
                    val identity = cellInfo.cellIdentity

                    if (identity.networkId != Integer.MAX_VALUE) {
                        mobileCountryCode = identity.networkId
                    }

                    if (identity.basestationId != Integer.MAX_VALUE) {
                        cellId = identity.basestationId
                    }
                }

                is CellInfoWcdma -> {
                    radioType = "wcdma"
                    signalStrength = cellInfo.cellSignalStrength.dbm
                    val identity = cellInfo.cellIdentity

                    if (identity.mcc != Integer.MAX_VALUE) {
                        mobileCountryCode = identity.mcc
                    }

                    if (identity.mnc != Integer.MAX_VALUE) {
                        mobileNetworkCode = identity.mnc
                    }

                    if (identity.lac != Integer.MAX_VALUE) {
                        locationAreaCode = identity.lac
                    }

                    if (identity.cid != Integer.MAX_VALUE) {
                        cellId = identity.cid
                    }
                }

                is CellInfoLte -> {
                    radioType = "lte"
                    signalStrength = cellInfo.cellSignalStrength.dbm
                    timingAdvance = cellInfo.cellSignalStrength.timingAdvance
                    val identity = cellInfo.cellIdentity

                    if (identity.mcc != Integer.MAX_VALUE) {
                        mobileCountryCode = identity.mcc
                    }

                    if (identity.mnc != Integer.MAX_VALUE) {
                        mobileNetworkCode = identity.mnc
                    }

                    if (identity.ci != Integer.MAX_VALUE) {
                        cellId = identity.ci
                    }
                }

                else -> {
                    radioType = "unknown"
                }
            }

            val cellDto = CellTowerRequestDto(
                radioType,
                mobileCountryCode,
                mobileNetworkCode,
                locationAreaCode,
                cellId,
                signalStrength,
                timingAdvance
            )
            cellTowers.add(cellDto)
        }
        return cellTowers
    }

    @SuppressLint("MissingPermission")
    private suspend fun getGPSInfo() = suspendCoroutine<Gps> { continuation ->
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    continuation.resume(
                        Gps(
                            latitude = location.latitude,
                            longitude = location.longitude,
                            accuracy = location.accuracy
                        )
                    )
                } else {
                    Toast.makeText(
                        context,
                        "Couldn’t get last known GPS position. No report was created.",
                        Toast.LENGTH_LONG
                    ).show()
                    context.stopSelf()
                }
            }
    }

    private fun prepareReport(
        wifiList: MutableList<WifiRequestDto>
    ) = GlobalScope.launch {
        val factory = ReportFactory()
        val gpsInfo = getGPSInfo()
        val cellTowerList = getCellTowerDataList(
            getSystemService(android.content.Context.TELEPHONY_SERVICE) as TelephonyManager
        )
        val repository = ReportRepository.getInstance(AppDatabase.getInstance(context).reportDao())
        val handler = Handler(Looper.getMainLooper())
        val gson = Gson()
        val mlsRequest = createMlsRequest(cellTowerList, wifiList)
        val locationResponseDto = handleMlsRequest(
            mlsRequest = mlsRequest,
            queue = Volley.newRequestQueue(applicationContext),
            url = getString(R.string.mlsUrl) + BuildConfig.MlsAPIKey,
            gson = Gson()
        )
        finalizeReport(factory, gpsInfo, mlsRequest, locationResponseDto, repository, handler, gson)
    }


    private fun finalizeReport(
        factory: ReportFactory,
        gpsInfo: Gps,
        mlsRequest: MlsRequestDto,
        locationResponseDto: LocationResponseDto,
        repository: ReportRepository,
        handler: Handler,
        gson: Gson
    ) {
        val mlsRequestJson = JSONObject(gson.toJson(mlsRequest)).toString(2)
        val mlsResultJson = JSONObject(gson.toJson(locationResponseDto)).toString(2)
        val report = factory.create(
            gps = gpsInfo,
            mls = Mls(
                locationResponseDto.location.lat,
                locationResponseDto.location.lng,
                locationResponseDto.accuracy
            ),
            mlsRequest = mlsRequestJson,
            mlsResult = mlsResultJson
        )

        CoroutineScope(Dispatchers.IO).launch {
            repository.insertReport(report)
        }

        handler.post {
            Toast.makeText(context, "The report was saved.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun createMlsRequest(
        cellTowerList: MutableList<CellTowerRequestDto>,
        wifiList: MutableList<WifiRequestDto>
    ): MlsRequestDto = MlsRequestDto(cellTowerList, wifiList)

    private suspend fun handleMlsRequest(
        mlsRequest: MlsRequestDto,
        queue: RequestQueue,
        url: String,
        gson: Gson
    ) = suspendCoroutine<LocationResponseDto> { continuation ->
        val mlsParams = JSONObject(gson.toJson(mlsRequest))
        val request = JsonObjectRequest(
            Request.Method.POST,
            url,
            mlsParams,
            Response.Listener { response ->
                val locationResponseDto = gson.fromJson(response.toString(), LocationResponseDto::class.java)
                continuation.resume(locationResponseDto)
            },
            Response.ErrorListener {
                Toast.makeText(context, "The MLS request failed. No report was created.", Toast.LENGTH_LONG).show()
                context.stopSelf()
            }
        )
        queue.add(request)
    }
}
