package gruppe2.mnsa2018w.mls.workers

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import gruppe2.mnsa2018w.mls.data.AppDatabase
import gruppe2.mnsa2018w.mls.data.Report
import gruppe2.mnsa2018w.mls.utilities.REPORT_FIXTURE_FILENAME

class SeedDatabaseWorker(context: Context, workerParams: WorkerParameters) : Worker(context, workerParams) {
    private val tag by lazy { SeedDatabaseWorker::class.java.simpleName }

    override fun doWork(): Result {
        val reportType = object : TypeToken<List<Report>>() {}.type
        var jsonReader: JsonReader? = null

        return try {
            val inputStream = applicationContext.assets.open(REPORT_FIXTURE_FILENAME)
            jsonReader = JsonReader(inputStream.reader())
            val reportList: List<Report> = Gson().fromJson(jsonReader, reportType)
            val database = AppDatabase.getInstance(applicationContext)
            database.reportDao().insertAll(reportList)
            Result.success()
        } catch (ex: Exception) {
            Log.e(tag, "Error seeding database", ex)
            Result.failure()
        } finally {
            jsonReader?.close()
        }
    }
}
