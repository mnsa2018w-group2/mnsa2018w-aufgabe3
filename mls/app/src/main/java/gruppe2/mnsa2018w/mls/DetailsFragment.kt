package gruppe2.mnsa2018w.mls

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.core.app.ShareCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import gruppe2.mnsa2018w.mls.data.Report
import gruppe2.mnsa2018w.mls.databinding.DetailsFragmentBinding
import gruppe2.mnsa2018w.mls.utilities.InjectorUtils
import gruppe2.mnsa2018w.mls.viewmodels.DetailsViewModel

class DetailsFragment : Fragment() {

    private lateinit var shareText: String
    private lateinit var reportDetailViewModel: DetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val reportId = DetailsFragmentArgs.fromBundle(arguments).reportId

        val factory = InjectorUtils.provideDetailsViewModelFactory(requireActivity(), reportId)
        reportDetailViewModel = ViewModelProviders.of(this, factory).get(DetailsViewModel::class.java)

        val binding = DataBindingUtil.inflate<DetailsFragmentBinding>(
            inflater, R.layout.details_fragment, container, false
        ).apply {
            viewModel = reportDetailViewModel
            setLifecycleOwner(this@DetailsFragment)
        }

        reportDetailViewModel.report.observe(this, Observer { report ->
            shareText = if (report == null) {
                ""
            } else {
                convertReportToShareText(report)
            }
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_report_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @Suppress("DEPRECATION")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_share -> {
                val shareIntent = ShareCompat.IntentBuilder.from(activity)
                    .setText(shareText)
                    .setType("text/plain")
                    .createChooserIntent()
                    .apply {
                        addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                    }
                startActivity(shareIntent)
                return true
            }
            R.id.action_delete -> {
                reportDetailViewModel.deleteReport()
                view!!.findNavController().navigateUp()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun convertReportToShareText(report: Report): String {
        val stringBuilder = StringBuilder()
        stringBuilder.appendln("Report: ${report.reportId}")
        stringBuilder.appendln("Created at: ${report.date}")
        stringBuilder.appendln("Coordinates: ${report.gps}")
        stringBuilder.appendln("Precision of the GPS: ${report.gps.accuracy}")
        stringBuilder.appendln("Difference between MLS and GPS: ${report.positionDifference}")
        stringBuilder.appendln("Parameters of MLS request: ${report.mlsRequest}")
        stringBuilder.appendln("Results of MLS request: ${report.mlsResult}")
        return stringBuilder.toString()
    }

}
