package gruppe2.mnsa2018w.mls.dtos

data class MlsRequestDto(
    val cellTowers: MutableList<CellTowerRequestDto>,
    val wifiAccessPointDtos: MutableList<WifiRequestDto>
)
