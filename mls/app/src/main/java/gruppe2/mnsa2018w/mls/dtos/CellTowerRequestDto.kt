package gruppe2.mnsa2018w.mls.dtos

data class CellTowerRequestDto(
    var radioType: String,
    var mobileCountryCode: Int,
    var mobileNetworkCode: Int,
    var locationAreaCode: Int,
    var cellId: Int,
    var signalStrength: Int,
    var timingAdvance: Int
)
