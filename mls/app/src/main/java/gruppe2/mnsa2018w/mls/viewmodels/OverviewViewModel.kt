package gruppe2.mnsa2018w.mls.viewmodels

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import gruppe2.mnsa2018w.mls.data.Report
import gruppe2.mnsa2018w.mls.data.ReportRepository
import gruppe2.mnsa2018w.mls.workers.SeedDatabaseWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class OverviewViewModel internal constructor(private val repository: ReportRepository) : ViewModel() {
    private val reportList = MediatorLiveData<List<Report>>()

    init {
        val liveReportList = repository.getReports()
        reportList.addSource(liveReportList, reportList::setValue)
    }

    fun getReports() = reportList

    private val viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     * Cancel all Coroutines when the ViewModel is cleared.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun reloadTestData() {
        viewModelScope.launch {
            val request = OneTimeWorkRequestBuilder<SeedDatabaseWorker>().build()
            WorkManager.getInstance().enqueue(request)
        }
    }

    fun deleteAllReports() {
        viewModelScope.launch {
            repository.removeAllReports()
        }
    }

}
