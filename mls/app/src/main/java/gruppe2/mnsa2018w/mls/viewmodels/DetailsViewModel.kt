package gruppe2.mnsa2018w.mls.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import gruppe2.mnsa2018w.mls.data.Report
import gruppe2.mnsa2018w.mls.data.ReportRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class DetailsViewModel internal constructor(
    private val reportRepository: ReportRepository,
    reportId: Int
) : ViewModel() {
    val report: LiveData<Report> = reportRepository.getReport(reportId)

    private val viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     * Cancel all Coroutines when the ViewModel is cleared.
     */
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun deleteReport() {
        viewModelScope.launch {
            reportRepository.removeReport(report.value!!)
        }
    }
}
