package gruppe2.mnsa2018w.mls

import android.Manifest
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import gruppe2.mnsa2018w.mls.databinding.MainActivityBinding
import gruppe2.mnsa2018w.mls.utilities.PERMISSION_COARSE_LOCATION
import gruppe2.mnsa2018w.mls.utilities.PERMISSION_FINE_LOCATION
import gruppe2.mnsa2018w.mls.utilities.REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS

class MainActivity : AppCompatActivity() {

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController
    private lateinit var mKeyguardManager: KeyguardManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_FINE_LOCATION
            )
        }

        if (!isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                PERMISSION_COARSE_LOCATION
            )
        }

        val binding: MainActivityBinding = DataBindingUtil.setContentView(this, R.layout.main_activity)
        drawerLayout = binding.drawerLayout

        navController = Navigation.findNavController(this, R.id.mls_nav_fragment)
        appBarConfiguration = AppBarConfiguration(navController.graph, drawerLayout)

        setSupportActionBar(binding.toolbar)
        setupActionBarWithNavController(navController, appBarConfiguration)

        binding.navigationView.setupWithNavController(navController)

        mKeyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val lockEnabled = sharedPreferences.getBoolean("credential_switch", false)

        if (lockEnabled && !mKeyguardManager.isKeyguardSecure) {
            Toast.makeText(
                this,
                "Secure lock screen hasn't set up.\n"
                        + "Go to 'Settings -> Security -> Screenlock' to set up a lock screen",
                Toast.LENGTH_LONG
            ).show()
            return
        } else if (lockEnabled) {
            showAuthenticationScreen()
        }
    }

    private fun showAuthenticationScreen() {
        val intent = mKeyguardManager.createConfirmDeviceCredentialIntent(null, null)
        if (intent != null) {
            startActivityForResult(intent, REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Authentication successful.", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Authentication failed.", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    class PreferencesFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.preferences, rootKey)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_FINE_LOCATION -> {
                when (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    true -> Toast.makeText(this, "Permission granted", Toast.LENGTH_LONG).show()
                    false -> Toast.makeText(this, "Permission not granted", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun isPermissionGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
    }

}
