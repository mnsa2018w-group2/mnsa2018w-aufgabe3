package gruppe2.mnsa2018w.mls.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import gruppe2.mnsa2018w.mls.OverviewFragmentDirections
import gruppe2.mnsa2018w.mls.data.Report
import gruppe2.mnsa2018w.mls.databinding.ListItemReportBinding

class OverviewAdapter : ListAdapter<Report, OverviewAdapter.ViewHolder>(ReportDiffCallback()) {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val report = getItem(position)
        holder.apply {
            bind(createOnClickListener(report.reportId), report)
            itemView.tag = report
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemReportBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    private fun createOnClickListener(reportId: Int): View.OnClickListener {
        return View.OnClickListener {
            val direction = OverviewFragmentDirections.ActionOverviewFragmentToDetailsFragment(reportId)
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val binding: ListItemReportBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: View.OnClickListener, item: Report) {
            binding.apply {
                clickListener = listener
                report = item
                executePendingBindings()
            }
        }
    }
}

private class ReportDiffCallback : DiffUtil.ItemCallback<Report>() {
    override fun areItemsTheSame(oldItem: Report, newItem: Report): Boolean {
        return oldItem.reportId == newItem.reportId
    }

    override fun areContentsTheSame(oldItem: Report, newItem: Report): Boolean {
        return oldItem == newItem
    }
}