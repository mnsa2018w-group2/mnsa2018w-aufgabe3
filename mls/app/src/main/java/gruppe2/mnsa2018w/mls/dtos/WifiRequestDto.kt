package gruppe2.mnsa2018w.mls.dtos

data class WifiRequestDto(
    var macAddress: String,
    var age: Number,
    var signalStrength: Number
)
