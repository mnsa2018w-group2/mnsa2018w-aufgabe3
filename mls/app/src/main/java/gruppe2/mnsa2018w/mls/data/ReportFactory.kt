package gruppe2.mnsa2018w.mls.data

import android.location.Location
import java.util.*

class ReportFactory {
    fun create(
        gps: Gps,
        mls: Mls,
        mlsRequest: String,
        mlsResult: String
    ): Report {
        return Report(
            date = Date().toString(),
            gps = gps,
            mls = mls,
            positionDifference = calculateDistance(gps, mls),
            mlsRequest = mlsRequest,
            mlsResult = mlsResult
        )
    }

    private fun calculateDistance(gps: Gps, mls: Mls): Float {
        val gpsLocation = Location("")
        gpsLocation.latitude = gps.latitude
        gpsLocation.longitude = gps.longitude

        val mlsLocation = Location("")
        mlsLocation.latitude = mls.latitude
        mlsLocation.longitude = mls.longitude

        return gpsLocation.distanceTo(mlsLocation)
    }
}