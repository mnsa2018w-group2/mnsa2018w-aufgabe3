package gruppe2.mnsa2018w.mls

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import gruppe2.mnsa2018w.mls.adapters.OverviewAdapter
import gruppe2.mnsa2018w.mls.databinding.OverviewFragmentBinding
import gruppe2.mnsa2018w.mls.services.LocationScanService
import gruppe2.mnsa2018w.mls.utilities.InjectorUtils
import gruppe2.mnsa2018w.mls.viewmodels.OverviewViewModel

class OverviewFragment : Fragment() {

    private lateinit var overviewViewModel: OverviewViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val factory = InjectorUtils.provideOverviewViewModelFactory(requireActivity())
        overviewViewModel = ViewModelProviders.of(this, factory).get(OverviewViewModel::class.java)

        val binding = DataBindingUtil.inflate<OverviewFragmentBinding>(
            inflater, R.layout.overview_fragment, container, false
        ).apply {
            setLifecycleOwner(this@OverviewFragment)
            addReportButton.setOnClickListener {
                startLocationScan()
            }
        }

        val adapter = OverviewAdapter()
        binding.reportList.adapter = adapter
        subscribeUi(adapter)

        setHasOptionsMenu(true)
        return binding.root
    }

    private fun startLocationScan() {
        val locationScanIntent = Intent(activity, LocationScanService::class.java)
        activity?.startService(locationScanIntent)
        Snackbar.make(view!!, "A location scan was started", Snackbar.LENGTH_LONG).show()
    }

    private fun subscribeUi(adapter: OverviewAdapter) {
        overviewViewModel.getReports().observe(viewLifecycleOwner, Observer { reports ->
            if (reports != null) adapter.submitList(reports)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_overview, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @Suppress("DEPRECATION")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_delete_all -> {
                overviewViewModel.deleteAllReports()
                Snackbar.make(view!!, getString(R.string.snackbar_reports_deleted), Snackbar.LENGTH_LONG).show()
                return true
            }
            R.id.action_reload_test_data -> {
                overviewViewModel.reloadTestData()
                Toast.makeText(context, getString(R.string.snackbar_testdata_reloaded), Toast.LENGTH_LONG).show()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
