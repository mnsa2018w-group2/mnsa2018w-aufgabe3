package gruppe2.mnsa2018w.mls.utilities

const val DATABASE_NAME = "mls-app"
const val REPORT_FIXTURE_FILENAME = "report-data-fixture.json"
const val REQUEST_CODE_CONFIRM_DEVICE_CREDENTIALS = 1
const val PERMISSION_FINE_LOCATION = 1
const val PERMISSION_COARSE_LOCATION = 2