package gruppe2.mnsa2018w.mls.utilities

import android.content.Context
import gruppe2.mnsa2018w.mls.data.AppDatabase
import gruppe2.mnsa2018w.mls.data.ReportRepository
import gruppe2.mnsa2018w.mls.viewmodels.DetailsViewModelFactory
import gruppe2.mnsa2018w.mls.viewmodels.OverviewViewModelFactory

/**
 * Static methods used to inject classes needed for various Activities and Fragments.
 * Source: https://github.com/googlesamples/android-sunflower/blob/master/app/src/main/java/com/google/samples/apps/sunflower/utilities/InjectorUtils.kt
 */
object InjectorUtils {
    private fun getReportRepository(context: Context): ReportRepository {
        return ReportRepository.getInstance(AppDatabase.getInstance(context).reportDao())
    }

    fun provideOverviewViewModelFactory(context: Context): OverviewViewModelFactory {
        val repository = getReportRepository(context)
        return OverviewViewModelFactory(repository)
    }

    fun provideDetailsViewModelFactory(
        context: Context,
        reportId: Int
    ): DetailsViewModelFactory {
        val repository = getReportRepository(context)
        return DetailsViewModelFactory(repository, reportId)
    }
}