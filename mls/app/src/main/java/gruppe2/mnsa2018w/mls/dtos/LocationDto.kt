package gruppe2.mnsa2018w.mls.dtos


data class LocationDto(val lat: Double, val lng: Double)
