package gruppe2.mnsa2018w.mls.data

import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class ReportRepository private constructor(private val reportDao: ReportDao) {
    fun getReports() = reportDao.getReports()

    fun getReport(reportId: Int) = reportDao.getReport(reportId)

    suspend fun insertReport(report: Report) {
        withContext(IO) {
            reportDao.insertReport(report)
        }
    }

    suspend fun removeReport(report: Report) {
        withContext(IO) {
            reportDao.deleteReport(report)
        }
    }

    suspend fun removeAllReports() {
        withContext(IO) {
            reportDao.deleteAllReports()
        }
    }

    // For singleton use
    companion object {
        @Volatile
        private var instance: ReportRepository? = null

        fun getInstance(reportDao: ReportDao) =
            instance ?: synchronized(this) {
                instance ?: ReportRepository(reportDao).also { instance = it }
            }
    }
}