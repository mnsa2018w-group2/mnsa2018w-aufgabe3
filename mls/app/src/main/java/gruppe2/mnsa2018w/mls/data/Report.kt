package gruppe2.mnsa2018w.mls.data

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "reports")
data class Report(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val reportId: Int = 0,
    val date: String,
    @Embedded(prefix = "gps_") val gps: Gps,
    @Embedded(prefix = "mls_") val mls: Mls,
    val positionDifference: Float,
    val mlsRequest: String,
    val mlsResult: String
)

data class Gps(
    val latitude: Double,
    val longitude: Double,
    val accuracy: Float
) {
    override fun toString() =
        "${"%.6f".format(latitude)}, ${"%.6f".format(longitude)}"
}

data class Mls(
    val latitude: Double,
    val longitude: Double,
    val accuracy: Float
) {
    override fun toString() =
        "${"%.6f".format(latitude)}, ${"%.6f".format(latitude)}"
}
