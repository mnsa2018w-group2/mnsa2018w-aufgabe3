package gruppe2.mnsa2018w.mls.dtos


data class LocationResponseDto(val location: LocationDto, val accuracy: Float)
