# Android MLS App

## AUFGABE A: Entwerfen Sie einen Mockup Ihrer App, welcher die Requirements abdeckt und den Android Guidelines für Material Design entspricht

### Die App muss folgende Funktionen anbieten

* Manuelle Durchführung einer Messung mittels MLS (Geolocate API), als Eingabedaten sollen sowohl wifiAccessPoints als auch cellTowers verwendet werden
* Automatische Speicherung der Messung und ihrer Ergebnisse in einem Bericht
* Anzeigen der Berichte (Übersicht/Detailansicht)
* Löschen eines Berichts
* Versenden eines Berichts per E-Mail (Bericht als Plaintext-Attachment) über einen Intent

### Jeder Messbericht muss zumindest folgende Daten enthalten

* Zeitpunkt
* Tatsächliche Koordinaten (GPS)
* Genauigkeit der GPS-Position (in m)
* Parameter sowie Ergebnis des MLS API Requests
* Unterschied zwischen MLS und GPS Position (Distanz in m)

## AUFGABE B: Implementieren Sie die App nach den Requirements und dem Mockup aus Aufgabe A

### Architekturanforderungen

* Verwenden Sie eine einzige Activity und mehrere Fragments
* Scannen der Mobilfunkmasten und WiFi Hotspots muss in einem IntentService stattfinden
* Verwenden Sie für die HTTP Kommunikation Retrofit oder Volley
* Verwenden Sie für Ihre Services (API Calls, GPS, ...) einen eventbasierten oder reaktiven Ansatz.

## AUFGABE C: Implementieren Sie geeignete Sicherheitsmaßnahmen (durch den Benutzer aktivierbar) um die Daten zu schützen und beschreiben Sie Ihre Implementierung im Projektbericht

Wie in der Vorlesung erläutert, ist der Schutz vertrauenswürdiger Daten in Apps nicht trivial. Sie wollen trotzdem folgende Daten vor unberechtigtem Auslesen schützen:

* Den MLS-API Key
* Die abgespeicherten Berichte

## AUFGABE D: Schreiben Sie automatisierte und nicht-triviale Tests für die geforderten Usecases. Achten Sie auf Unabhängigkeit der Testcases untereinander

Implementieren Sie folgende Tests automatisiert (d.h. die Interaktionen müssen von einem Testframework am Device ohne Interaktion durch den Nutzer beliebig oft in zufälliger Reihenfolge ausgeführt werden):

* Durchführung und Anzeige einer Messung (die Testdaten können gemocked werden)
* Anzeige der Liste von gespeicherten Messberichten (inkl. Scrolling)
* Öffnen der Detailansicht eines Messberichts
* Löschen eines Berichts

Mocken Sie die Testdaten wo sinnvoll (also Messergebnis, Liste der gespeicherten Testberichte). Sie können dafür z.B. Mockito verwenden. Verwenden Sie als Testsuite ein geeignetes Framework (z.B. Barista oder Espresso).

## AUFGABE E: Führen Sie mit Ihrer App Messungen an drei beliebigen Messpunkten durch und dokumentieren Sie diese

Die Messpunkte sind von Ihnen frei wählbar, sie müssen allerdings einen Mindestabstand von 500m Luftlinie voneinander haben um aussagekräftig zu sein. Dokumentieren Sie jeden Messpunkt mit den Daten aus Ihrer App und zusätzlich mit einem Foto der Messposition.

## Abgabedokument

* Ihre Mockup(s) aus Aufgabe A inkl. kurzer Beschreibung der dargestellten Inhalte
* Mindestens. einen Screenshot Ihrer App
* Eine kurze aber präzise Erläuterung Ihrer Securitymaßnahmen aus Aufgabe C
* Den Messbericht aus Aufgabe E
* Einen kurzen Projektbericht (~1/2 Seite) in dem Sie Ihre Erfahrungen mit dem Übungsbeispiel schildern

Achtung: Auf keinen Fall den build-Ordner ins `.zip` inkludieren.
